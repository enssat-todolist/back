# ToDo list API

## What it does ##
* Allows users to identify themselves with a google account
* Create and manage a list of tasks
* See tasks to do

## About the author ##
Kévin Salaün
ENSSAT's student
IMR2 (2018-2019)