const Settings = {
    prod_mod: false,
    debug: {
        storage: {
            dialect: "sqlite",
            storage: ":memory:",
            logging: true
        },
        default_user: {
            id:       1,
            username: 'sam',
            password: 'toto'
        }
    },
    prod: {
        /*host    : 'remotemysql.com',
        port    : '3306',
        user    : '0jllZ0Pj7d',
        password: '6N5zkvtOrW',
        database: '0jllZ0Pj7d',*/
        host    : 'ec2-54-228-252-67.eu-west-1.compute.amazonaws.com',
        port    : '5432',
        user    : 'siwxzrngfjeggl',
        password: '728e9338fffd62a9e750ae14bb106305c250045dc4d68ab6be015eef027b2cf5',
        database: 'ddeglh0e8nmt2g',
        dialect : 'mysql', // or 'mariadb' , 'postgres', 'mssql', 'sqlite'
        logging : false
    },
    exclude_models: ['StatusEnum', 'TagsEnum']
}

module.exports = Settings;