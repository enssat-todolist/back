var express = require('express');
var router = express.Router();

/* redirect to doc page. */
router.get('/', function(req, res, next) {
  newURL = req.protocol + '://' + req.headers.host + '/api/v1/';
  res.redirect(newURL);
});

console.log('index router loaded');
module.exports = router;
