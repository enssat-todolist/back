const models  = require('../models');
const express = require('express');
const router  = express.Router();
const config  = require('../config/config');
const StatusEnum  = require('../models/StatusEnum');
const TagsEnum  = require('../models/TagsEnum');

// debug mode
var userLogged = config.debug.default_user; //To be changed when authentication will be implemented.

function TaskSerialize(obj) {
  var statusList = StatusEnum.toList();
  var tagsList = TagsEnum.toList();
  var objt = {
    id: obj.id,
    title: obj.title,
    dateBegin: obj.dateBegin,
    dateEnd: obj.dateEnd,
    status: obj.status,
    tags: obj.tags,
    participants: obj.participants
  }
  if(parseInt(objt.id) == 'NaN') {
    objt.id = null;
  }
  try {
    new Date(objt.dateBegin);
  } catch (error) {
    objt.dateBegin = null;
  }
  try {
    new Date(objt.dateEnd);
  } catch (error) {
    objt.dateEnd = null;
  }
  var status = null
  for(var idx in statusList) {
    if(objt.status.id != undefined) {
      if(objt.status.id == statusList[idx].id) {
        status = statusList[idx].id;
      }
    } else {
      if(objt.status == statusList[idx].id) {
        status = statusList[idx].id;
      }
    }
  }
  objt.status = status;
  var tagsParsed = objt.tags.trim().replace(/\s+/g, ' ').split(' ');
  var tags = [];
  for(var idx in tagsParsed) {
    for(var idx2 in tagsList) {
      if(tagsParsed[idx] == tagsList[idx2].id) {
        tags.push(tagsParsed[idx]);
      }
    }
  }
  objt.tags = tags.join(' ');
  for(var idx in objt) {
    if(typeof objt[idx] === 'undefined') {
      objt[idx] = null;
    }
  }
  return objt;
}

/**
 * @typedef Task
 * @property {integer} id.required - id
 * @property {string} title.required - title
 * @property {string} dateBegin.required - start date of the task
 * @property {string} dateEnd.required - end date of the task
 * @property {StatusEnum} status.required - status
 * @property {Array.<string>} tags.required - list of tags describing the task
 */

/**
 * Lists all tasks.
 * @route GET /tasks
 * @group tasks - Operations about tasks
 * @returns {object} 200 - A Json of tasks listing.
 */
router.get('/tasks', function(req, res, next) {
  models.Task.findAll({
    where: {
      UserId: userLogged.id
    }
  }).then((tasks) => {
    // console.log("All Tasks: ", JSON.stringify(tasks, null, 4));
    res.json(tasks);
  }).catch((error) => {
    res.json({error: 'an error has occurred: '+error});
  });
});

/**
 * Creates a new task.
 * @route PUT /tasks
 * @group tasks - Operations about tasks
 * @param {Task} task.body.required - the task to add in database.
 * @returns {object} 200 - return nothing.
 */
router.put('/tasks', function(req, res, next) {
  var task = TaskSerialize(req.body);
  task.UserId = userLogged.id;
  delete task.id;
  //
  models.Task.create(task)
  .then((task) => {
    res.json(task);
  }).catch((error) => {
    res.json({error: 'an error has occurred: '+error});
  });
});

/**
 * Lists all tasks respecting a filter by tags.
 * @route GET /tasks/bytags/:tags
 * @group tasks - Operations about tasks
 * @returns {object} 200 - A Json of tasks listing.
 */
router.get('/tasks/bytags/:tags', function(req, res, next) { ///////////////////////
  var tagsProvided = req.params.tags.trim().replace(/\s+/g, ' ').split(' ');
  var filter = [];
  for(var idx in tagsProvided) {
    filter.push(
      {tags: {[models.Sequelize.Op.like]: '%'+tagsProvided[idx]+'%'}}
    );
  }
  //
  models.Task.findAll({
    where: {
      UserId: userLogged.id,
      [models.Sequelize.Op.or]: filter
    }
  }).then((tasks) => {
    // console.log("All Tasks for tags ", tagsProvided, ": ", JSON.stringify(tasks, null, 4));
    res.json(tasks);
  }).catch((error) => {
    res.json({error: 'an error has occurred: '+error});
  });
});

/**
 * Lists all tasks respecting a filter by status.
 * @route GET /tasks/bystatus/:status
 * @group tasks - Operations about tasks
 * @returns {object} 200 - A Json of tasks listing.
 */
router.get('/tasks/bystatus/:status', function(req, res, next) { ///////////////////////
  var statusProvided = req.params.status.trim().replace(/\s+/g, ' ').split(' ');
  var filter = [];
  for(var idx in statusProvided) {
    filter.push(
      {status: {[models.Sequelize.Op.like]: '%'+statusProvided[idx]+'%'}}
    );
  }
  //
  models.Task.findAll({
    where: {
      UserId: userLogged.id,
      [models.Sequelize.Op.or]: filter
    }
  }).then((tasks) => {
    // console.log("All Tasks for status ", statusProvided, ": ", JSON.stringify(tasks, null, 4));
    res.json(tasks);
  }).catch((error) => {
    res.json({error: 'an error has occurred: '+error});
  });
});

/**
 * Provides the list of status.
 * @route GET /tasks/statuslist
 * @group tasks - Operations about tasks
 * @returns {object} 200 - return the list of status, a dict {id: '', value: ''}.
 */
router.get('/tasks/statuslist', function(req, res, next) { ///////////////////////
  const response = StatusEnum.toList();
  console.log('Liste des status: '+response);
  res.json(response);
});

/**
 * Provides the list of tags.
 * @route GET /tasks/tagslist
 * @group tasks - Operations about tasks
 * @returns {object} 200 - return the list of tags.
 */
router.get('/tasks/tagslist', function(req, res, next) { ///////////////////////
  const response = TagsEnum.toList();
  console.log('Liste des tags: '+response);
  res.json(response);
});

/**
 * Reset database.
 * @route GET /tasks/reset
 * @group tasks - Operations about tasks
 * @returns {object} 200 - return nothing.
 */
router.get('/tasks/reset', function(req, res, next) {
  models.sequelize.sync({ force: true }).then(() => {
    // console.log('Database was purged');
    res.json({status: 1});
  }).catch((error) => {
    res.json({error: 'an error has occurred: '+error});
  });
});

/**
 * Returns the details of a task.
 * @route GET /task/:id
 * @group tasks - Operations about tasks
 * @param {integer} id.query.required - id of the task you want to consult.
 * @returns {object} 200 - return the task corresponding to the id.
 */
router.get('/task/:id', function(req, res, next) {
  const tId = req.params.id;
  //
  models.Task.findOne({
    where: {
      UserId: userLogged.id,
      id: tId
    }
  }).then((task) => {
    // console.log("Task #", tId, ": ", task.title);
    res.json(task);
  }).catch((error) => {
    res.json({error: 'an error has occurred: '+error});
  });
});

/**
 * Deletes a task.
 * @route DELETE /task/:id
 * @group tasks - Operations about tasks
 * @param {integer} id.query.required - id of the task you want to delete.
 * @returns {object} 200 - return nothing.
 */
router.delete('/task/:id', function(req, res, next) {
  const tId = req.params.id;
  //
  models.Task.destroy({
    where: {
      UserId: userLogged.id,
      id: tId
    }
  }).then((data) => {
    // console.log("Task #", id, " deleted");
    res.json({status: data});
  }).catch((error) => {
    res.json({error: 'an error has occurred: '+error});
  });
});

/**
 * Replaces old information on a task with new information.
 * @route POST /task/:id
 * @group tasks - Operations about tasks
 * @param {integer} id.query.required - id of the task you want to update.
 * @param {Task} task.body.required - the new task object.
 * @returns {object} 200 - return nothing.
 */
router.post('/task/:id', function(req, res, next) { //keep the id provided in the url
  req.body.id = req.params.id;
  const newTask = TaskSerialize(req.body);
  //
  models.Task.update(newTask, { //or upsert
    where: {
      UserId: userLogged.id,
      id: newTask.id
    }
  }).then((data) => {
    // console.log("Task #", newTask.id, " was updated");
    res.json({status: data[0]});
  }).catch((error) => {
    res.json({error: 'an error has occurred: '+error});
  });
});

console.log('tasks router loaded');
module.exports = router;
