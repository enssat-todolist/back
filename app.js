'use strict';

const createError  = require('http-errors');
const express      = require('express');
const path         = require('path');
const cookieParser = require('cookie-parser');
const logger       = require('morgan');
const bodyParser   = require('body-parser');
const favicon      = require('serve-favicon');

const indexRouter = require('./routes/index');
const tasksRouter = require('./routes/tasks');

const app = express();
const expressSwagger = require('express-swagger-generator')(app);

// swagger configuration
app.swaggerOptions = {
  swaggerDefinition: {
      info: {
          description: 'The todo list app server',
          title: 'TodoList',
          version: '1.0.0',
      },
      host: '0.0.0.0:5000',
      basePath: '/api/v1',
      produces: [
          "application/json",
          "application/xml"
      ],
      schemes: ['http', 'https'],
      securityDefinitions: {
          JWT: {
              type: 'apiKey',
              in: 'header',
              name: 'Authorization',
              description: "",
          }
      }
  },
  basedir: __dirname, //app absolute path
  route: {
    url: '/api/v1/',
    docs: '/api/v1/.json'
  },
  files: ['./routes/*.js'] //Path to the API handle folder  // './models/**/*.js' ?
};
expressSwagger(app.swaggerOptions)

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(favicon(__dirname + '/public/images/favicon.ico'));

app.use('/', indexRouter);
app.use('/api/v1/', tasksRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;