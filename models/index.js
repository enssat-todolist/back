'use strict';

var fs        = require('fs');
var path      = require('path');
var Sequelize = require('sequelize');
var basename  = path.basename(__filename);
var config    = require('../config/config');
var db        = {};

var sequelize;
if(config.prod_mod) {
  sequelize = new Sequelize(config.prod.database, config.prod.username, config.prod.password, config.prod);
} else {
  sequelize = new Sequelize(config.debug.storage);
}

fs
  .readdirSync(__dirname)
  .filter(file => {
    for(var idx in config.exclude_models) {
      if(file.slice(0, -3) === config.exclude_models[idx])
        return false;
    }
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    var model = sequelize['import'](path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;