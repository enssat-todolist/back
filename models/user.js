'use strict';

/**
 * @typedef User
 * @property {integer} id.required - id
 * @property {string} username.required - username
 * @property {string} password.required - password
 * @property {string} token.required - session token (possibly to move)
 * @property {datetime} token_timeout.required - token expiration
 */
module.exports = (sequelize, DataTypes) => {
    var User = sequelize.define('User', {
        username: {
            type: DataTypes.STRING,
            allowNull: false
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false
        },
        token: {
            type: DataTypes.STRING
        },
        token_timeout: {
            type: DataTypes.DATE
        }
    });

    User.associate = function(models) {
        models.User.hasMany(models.Task);
    };

    return User;
};