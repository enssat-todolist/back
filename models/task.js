'use strict';

/**
 * @typedef Task
 * @property {integer} id.required - id
 * @property {string} title.required - title
 * @property {datetime} dateBegin.required - start date of the task
 * @property {datetime} dateEnd.required - end date of the task
 * @property {StatusEnum} status.required - status
 * @property {Array.<string>} tags.required - list of tags describing the task
 */
module.exports = (sequelize, DataTypes) => {
    var Task = sequelize.define('Task', {
        title: {
            type: DataTypes.STRING,
            allowNull: false
        },
        dateBegin: {
            type: DataTypes.DATE,
            allowNull: false
        },
        dateEnd: {
            type: DataTypes.DATE,
            allowNull: false
        },
        status: {
            type: {
                type: DataTypes.ENUM('Unknown', 'Waiting', 'OnGoing', 'Done', 'Canceled'),
                defaultValue: 'Unknown'
            },
            allowNull: false
        },
        tags: {
            type: DataTypes.STRING, //DataTypes.ARRAY(DataTypes.STRING),
            allowNull: false
        }
    });

    Task.associate = function (models) {
        models.Task.belongsTo(models.User, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull: false
            }
        });
    };

    return Task;
};

/*
if(StatusEnum.parse(obj.status) == null) {
            console.log('Error status format');
            return null;
        }
*/