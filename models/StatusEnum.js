class StatusEnum {
    static Unknown()  { return {id: 'Unknown', value: "non précisé"}; }
    static Waiting()  { return {id: 'Waiting', value: "une tâche est recquise"}; }
    static OnGoing()  { return {id: 'OnGoing', value: "en cours"}; }
    static Done()     { return {id: 'Done', value: "achevé"}; }
    static Canceled() { return {id: 'Canceled', value: "annulé"}; }

    static toList() {
        const exceptions = ['toList', 'parse'];
        const all = [];
        Object.getOwnPropertyNames(StatusEnum).forEach(
            function(val, idx, array) {
                if(typeof StatusEnum[val] === "function" && exceptions.indexOf(val) == -1) {
                    all.push(StatusEnum[val]());
                }
            }
        );
        return all;
    }

    static parse(thetype) {
        const exceptions = ['toList', 'parse'];
        var result = null;
        Object.getOwnPropertyNames(StatusEnum).forEach(
            function(val, idx, array) {
                if(typeof StatusEnum[val] === "function" && val == thetype.id) {
                    result = StatusEnum[val]();
                    return;
                }
            }
        );
        /*const all = Object.getOwnPropertyNames(StatusEnum).filter(prop => typeof StatusEnum[prop] === "function" && exceptions.indexOf(prop) == -1);
        if(all.indexOf(thetype.id) != -1) {
            return true;
        }*/
        return result;
    }
}
module.exports = StatusEnum;
