class TagsEnum {
    static Personnal()  { return {id: 'Personnal', value: "personnel"}; }
    static Work()  { return {id: 'Work', value: "travail"}; }
    static Associative()  { return {id: 'Associative', value: "associatif"}; }

    static toList() {
        const exceptions = ['toList', 'parse'];
        const all = [];
        Object.getOwnPropertyNames(TagsEnum).forEach(
        function(val, idx, array) {
            if(typeof TagsEnum[val] === "function" && exceptions.indexOf(val) == -1) {
                all.push(TagsEnum[val]());
            }
        });
        return all;
    }

    static parse(thetype) {
        const exceptions = ['toList', 'parse'];
        const all = Object.getOwnPropertyNames(TagsEnum).filter(prop => typeof TagsEnum[prop] === "function" && exceptions.indexOf(prop) == -1);
        if(all.indexOf(thetype) != -1) {
            return true;
        }
        return false;
    }
}
module.exports = TagsEnum;
