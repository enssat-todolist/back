#!/usr/bin/env node

/**
 * Module dependencies.
 */

var app      = require('../app');
var debug    = require('debug')('back:server');
var http     = require('http');
var models   = require("../models");
const config = require('../config/config');
const faker  = require('faker');

/**
 * Get port from environment and store in Express.
 */

var port = normalizePort(process.env.PORT || '5000');
app.set('port', port);

/**
 * Create HTTP server.
 */

var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
// sync() will create all table if they doesn't exist in database
models.sequelize.sync().then(function () {
  // debug mode
  var userLogged = config.debug.default_user; //To be changed when authentication will be implemented.
  if(!config.prod_mod) {
    var getRandom = function(min, max) {
      return parseInt(Math.random() * (max - min) + min);
    }
    var getRandomTask = function() {
      var statusList = ['Unknown', 'Waiting', 'OnGoing', 'Done', 'Canceled'];
      var status = statusList[getRandom(0, statusList.length-1)];
      var tags = ['Personnal', 'Work', 'Associative'];
      delete tags[getRandom(0, tags.length-1)];
      tags = tags.join(' ').trim().replace(/\s+/g, ' ');
      return {
        title: faker.lorem.sentence(),
        dateBegin: faker.date.past(),
        dateEnd: faker.date.future(),
        status: status,
        tags: tags,
        participants: null
      };
    }
    // Create the user of test
    models.User.create(userLogged).then(function() {
      // Generate some tasks
      for(var i=0; i<10; i++) {
        var task = getRandomTask();
        task.UserId = userLogged.id;
        models.Task.create(task);
      }
    });
    // Create (random) another user of test
    var random_user = {
      username: faker.lorem.words()[0],
      password: 'toto'
    }
    models.User.create(random_user).then((user) => {
      // Generate some tasks
      for(var i=0; i<10; i++) {
        var task = getRandomTask();
        task.UserId = user.id;
        models.Task.create(task);
      }
    });
  }
  // Server launching
  server.listen(port);
  server.on('error', onError);
  server.on('listening', onListening);
  var host = (server.address().address.indexOf(':') != -1) ? '['+server.address().address+']' : server.address().address;
  app.swaggerOptions.swaggerDefinition.host = host+':'+port
  console.log('\nlistening on http://'+host+':'+port+'/');
  console.log('documentation available on http://'+host+':'+port+'/api-docs\n')
});

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}
